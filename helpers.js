const nodemailer = require('nodemailer')
const path = require('path');
const ejs = require('ejs');


const checkEmail = (email) => {
    var emailFormat = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    if (email !== '' && email.match(emailFormat)) { return true; }

    return false;
}
const phoneNumber = (phonenumber) => {

    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (phonenumber.match(phoneno)) {
        return true;
    }
    else {

        return false;
    }

}
const passwordValidation = (password) => {
    var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
    if (password.match(reg)) {
        return true;
    } else {
        return false;
    }
}

const welcomeMail = async (name, recipient) => {

    let mailTransporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        requireTLS: true,

        auth: {
            // user:'deepujoseph98@yahoo.com',
            // pass:'#nSHd(8k5A@jcBv'
            user: 'deepu.joseph.tblr@gmail.com',
            pass: 'srdxmjgdktxcjglh'


        }
    });

    const template = path.join(__dirname,'./views/mail.ejs')
    const data=await ejs.renderFile(template,{name})

    let mailDetails={
        from:'deepu.joseph.tblr@gmail.com',
        to:recipient,
        subject:'Welcome Mail',
        html:data
       }
       mailTransporter.sendMail(mailDetails,function(err,data){
        if(err){
          console.log(err)
  
        }else{
          console.log("Mail Sent",data.messageId)
  
        }
       })

}

module.exports = {
    checkEmail,
    phoneNumber,
    passwordValidation,
    welcomeMail
}
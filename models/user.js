const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    first_name:{
        type:String,
        required: [true, 'Please add a first name']
    },
    last_name:{
        type:String,
        required:[true,'Please add a last name']
    },
    email:{
        type:String,
        required:[true,'Please add a email']

    },
    phone_number:{
        type:String,
        required:[true,'Please add a phone_number']
    },
    password:{
        type:String,
        required:[true,'Please add password']

    },
    image:{
        type:String

    },
    subscribe:{
        type:Boolean,
        default:false
    },
    status:{
        type:String,
        enum:['Active','Inactive','Trash'],
        required:[true,'Please add a status'],
        default:'Active'
    },
    role_id:{
        type:mongoose.Schema.Types.ObjectId,
        required:[true,'Please Provide a role_id'],
        ref:'Roles'
    },
    user_profile:{
        type:String
    }

},{
    timestamps:true
})
module.exports = mongoose.model('Users', userSchema);

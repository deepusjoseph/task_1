const mongoose = require('mongoose');
const roleSchema = mongoose.Schema({
    role_name: {
        type: String,
        required: [true, 'Please add a name']
     
    }
},
    {
        timestamps: true
    }

)


module.exports = mongoose.model('Roles', roleSchema);
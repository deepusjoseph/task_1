
const express = require('express');
const path = require('path')
const bodyparser = require('body-parser');
const app = express();

const connectDb = require('./config/database')
connectDb()

//set views file
app.set('views', path.join(__dirname, 'views'))

//set view engine
app.set('view engine', 'ejs');

app.use(express.static(__dirname+'/public'));


app.use(express.json())
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true}));

app.use('/api/roles', require('./routes/role'));
app.use('/api/users', require('./routes/user'));
app.use('/api/login',require('./routes/auth'));
app.use('/',require('./routes/view'))


// app.get('/us',(req,res)=>[
//        res.render('table.ejs')
    
      
// ])
  





app.listen(5000, () => console.log('Server Running at Port 5000'))
const mongoose = require('mongoose')
const Roles = require('../models/role');
const { body, validationResult } = require('express-validator')

const getRole = async (req, res) => {
    try {
        const role = await Roles.find()
        res.status(200).json(role);

    } catch (error) {
        console.log(error.errors, "#43")


    }


}
const createRole = async (req, res) => {


    try {





        const role = req.body;

        const roleExist = await Roles.find({ role_name: role.role_name });

        if (roleExist) {
            res.status(400).json('Role Aready exist')
        }
        const newRole = new Roles(role);
        await newRole.save();
        res.status(200).json(newRole)


    } catch (error) {
        let erroMap = [];
        if (error.errors) {
            Object.values(error.errors).map(val => erroMap.push(val.message))

        }


        res.status(400).json({ erroMap })




    }
}
const updateRole = async (req, res) => {
    const { id: _id } = req.params;
    const role = req.body;
    if (Object.keys(role).length === 0) {
        res.status(400).json({ message: "Please add role" })

    }

    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No role with that id');
    try {
        const updatedRole = await Roles.findByIdAndUpdate({ _id }, role, { new: true })
        res.status(200).json({ updatedRole })



    } catch (error) {


    }
}
const deleteRole = async (req, res) => {
    const { id: _id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No role with that id');
    try {
        await Roles.findByIdAndRemove(_id);
        res.status(200).json(_id)


    } catch (error) {
        console.log(error)

    }
}
const getSingleRole = async (req, res) => {
    try {
        const { id: _id } = req.params;
        const roleDetail = await Roles.findById(_id)
        res.status(200).json({ roleDetail })

    } catch (error) {
        console.log(error)

    }

}

module.exports = {
    getRole,
    createRole,
    updateRole,
    deleteRole,
    getSingleRole
}
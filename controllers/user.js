const mongoose = require('mongoose');
const Users = require('../models/user');
const bcrypt = require('bcryptjs')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy

const { body, validationResult } = require('express-validator')
const { checkEmail, phoneNumber, welcomeMail } = require('../helpers');
const { db } = require('../models/user');

const nodemailer = require('nodemailer');







const getUser = async (req, res) => {
  try {
    const limit = req.query.limit;
    const page = req.query.page;
    let criteria = {};


    if (req.query.search) {
      criteria = {
        $or: [
          { first_name: { $regex: `${req.query.search}` } },
          { last_name: { $regex: `${req.query.search}` } },
          { email: { $regex: `${req.query.search}` } },

        ]
      }

    }
    if (req.query.status) {
      criteria['status'] = req.query.status
    }
    if (req.query.role_id) {
      criteria['role_id'] = req.query.role_id
    }

    const users = await Users.find(criteria).populate("role_id").limit(limit).skip((page - 1) * limit);
    const tradesCollectionCount = await Users.count()
    const totalPages = Math.ceil(tradesCollectionCount / limit)
    const currentPage = page
    res.status(200).json({
      data: users,
      paging: {
        total: tradesCollectionCount,
        page: currentPage,
        pages: totalPages,
      }
    })

  } catch (error) {
    console.log(error)

  }
}
const createUser = async (req, res) => {


  try {

    var isValid = true
    var phoneValid = true


    if (req.body.email.length !== 0) {


      isValid = checkEmail(req.body.email)

    }


    if (req.body.phone_number.length !== 0) {
      phoneValid = phoneNumber(req.body.phone_number)

    }

    if (!isValid && !phoneValid) {
      res.status(400).json({ "message": "Email is not valid & Phone number is not valid" })

    } else if (!isValid) {
      res.status(400).json({ "message": "Email is not valid" })

    } else if (!phoneValid) {
      res.status(400).json({ "message": "Phone number is not valid" })

    } else {


      const hashedPassword = await bcrypt.hash(req.body.password, 12);
      const user = req.body;

      if (req.file) {

        user['image'] = req.file.filename

      }
      user['password'] = hashedPassword

      const newUser = new Users(user);
      await newUser.save()

      await welcomeMail(newUser.first_name, newUser.email)

      res.status(200).json(newUser)






    }




  } catch (error) {

    let erroMap = [];

    Object.values(error.errors).map(val => erroMap.push(val.message))
    res.status(400).json({ erroMap })





  }
}
const updateUser = async (req, res) => {


  try {
    const { id: _id } = req.params;
    const users = req.body;
    const { first_name, last_name, email, phone_number } = req.body;
    let err = []
    if (!first_name && first_name == "") {
      err.push("First name is required")

    } else if (!last_name && last_name == "") {
      err.push("Last name is required")

    } else if (!email && email) {
      err.push("Email is required")

    } else if (!phone_number && phone_number) {
      err.push("Phone number is required")

    } else if (Object.keys(req.body).length === 0) {
      err.push("Please add some fields")

    }
    if (err.length > 0) {
      res.status(400).json({ err })
    }
    var isValid = true
    var phoneValid = true

    if (email.length !== 0) {


      isValid = checkEmail(email)

    }


    if (phone_number.length !== 0) {
      phoneValid = phoneNumber(phone_number)

    }

    if (!isValid && !phoneValid) {
      res.status(400).json({ "message": "Email is not valid & Phone number is not valid" })

    } else if (!isValid) {
      res.status(400).json({ "message": "Email is not valid" })

    } else if (!phoneValid) {
      res.status(400).json({ "message": "Phone number is not valid" })

    } else {
      const updateUser = await Users.findByIdAndUpdate({ _id }, { ...users, _id }, { new: true })

    }

    res.status(200).json({ updateUser })

  } catch (error) {
    res.status(400).json({ error })

  }
}
const deleteUser = async (req, res) => {
  try {
    const { id } = req.params;
    await Users.findByIdAndDelete(id);
    res.status(200).json({ id })

  } catch (error) {
    console.log(error)

  }
}
const getSingleUser = async (req, res) => {
  try {
    const { id: _id } = req.params;
    const userDetail = await Users.findById(_id)
    res.status(200).json({ userDetail })

  } catch (error) {
    console.log(error)

  }

}
const userList = async (req, res) => {
  const data = await Users.find()

  res.render('table.ejs', {
    data: data
  })
}

//Passport//

let userExists;

function initialize(passport) {

  var authenticateUser = async (email, password, done) => {

    userExists = await Users.findOne({ email: email }).exec();

    if (userExists == null) {
      return done(null, false, { message: 'No user with that email' })
    }

    try {
      if (await bcrypt.compare(password, userExists.password)) {
        return done(null, userExists)
      } else {
        return done(null, false, { message: 'Password incorrect' })
      }
    } catch (e) {

      return done(e)
    }


  }



  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser))
  passport.serializeUser((user, done) => done(null, user.id))
  passport.deserializeUser((id, done) => { return done(null, Users.findById(id)) })
}

module.exports = {
  getUser,
  createUser,
  updateUser, deleteUser,
  getSingleUser,
  initialize,
  userList

}
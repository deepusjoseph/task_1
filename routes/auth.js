const express = require('express');
// const passport = require('passport');
// const LocalStratergy = require('passport-local');
const bcrypt = require('bcryptjs');
const Users = require('../models/user');

// passport.use(new LocalStratergy(function verify(email, password, cb) {
//     console.log(email,"Sdsd")
//     Users.findById({ email }, function (err, user) {
//         if (err) { return cb(err) }
//         if (!user) {
//             return cb(null, false, { message: 'Incorrect Username and poassword' })
//         }
//         try {
//             if(await bcrypt.compare(password, user.password)){
//                 return cb(null, user)
//             } else{
//                 return cb (null, false, {message: "Password Incorrect"})
//             }
//         } catch (e) {
//             console.log(e);
//             return done(e)
//         }
        
//     });

// }))


// passport.serializeUser(function (user, cb) {
//     process.nextTick(function () {
//         cb(null, { id: user.id, username: user.username });
//     });
// });

// passport.deserializeUser(function (user, cb) {
//     process.nextTick(function () {
//         return cb(null, user);
//     });
// });
 const router = express.Router();

// router.post('/passport', passport.authenticate('local', {
//     successReturnToOrRedirect: '/api/roles/',
//     failureRedirect: '/api/users/',
//     failureMessage: true
// }));

////  PASSPORT  ////

const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')
const methodOverride = require('method-override')
const userController = require('../controllers/user')

userController.initialize(passport);
router.use(flash())
router.use(session({
  secret: "reallysecret",
  resave: false,
  saveUninitialized: false
}))
router.use(passport.initialize())
router.use(passport.session())
router.use(methodOverride('_method'))


router.post('/passport', passport.authenticate('local', {
    successRedirect: '/api/users',
   failureRedirect: '/api/roles',
    failureFlash: true
}))
function checkNotAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
      return res.redirect('/users/welcome')
    }
    next()
  }
  

 module.exports = router;
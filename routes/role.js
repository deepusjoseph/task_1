const express = require('express');
const router= express.Router(); 
const {getRole,createRole,updateRole,deleteRole,getSingleRole}=require('../controllers/role')


router.get('/',getRole);
router.get('/:id',getSingleRole);
router.post('/create',createRole);
router.post('/update/:id',updateRole);
router.post('/delete/:id',deleteRole);

module.exports=router;
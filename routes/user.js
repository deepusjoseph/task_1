const express = require('express');
const router = express.Router();
const { getUser, createUser, updateUser, deleteUser, getSingleUser,userList } = require('../controllers/user');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});


const upload = multer({
    storage: storage,
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb);
    }
});
// Check File Type
function checkFileType(file, cb) {
    // Allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Images Only!');
    }
}


router.get('/', getUser);
router.get('/:id', getSingleUser);
router.post('/create', upload.single('profile_image'), createUser);
router.post('/update/:id', updateUser);
router.post('/delete/:id', deleteUser);

router.get('/userList',userList)


module.exports = router;
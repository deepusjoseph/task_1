const express = require('express');
const router = express.Router();
const { userList} = require('../controllers/user');

router.get('/', userList);


module.exports = router;